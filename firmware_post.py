Import("env")

# Create a file with assembler listing
# arm-none-eabi-objdump --source --all-headers --demangle --disassemble --line-numbers --wide "CRDSTM32L.elf" > "CRDSTM32L.lst"
env.AddPostAction(
    "$BUILD_DIR/${PROGNAME}.elf",
    env.VerboseAction(" ".join([
        "arm-none-eabi-objdump", "--source", "--all-headers", "--demangle", "--disassemble", "--line-numbers", "--wide",
        "$TARGET", ">", "$BUILD_DIR/${PROGNAME}.lst"
    ]), "Building $BUILD_DIR/${PROGNAME}.lst")
)
